/* 
  pt.c
  very short program to demonstrate the return value of printf 
*/

#include <stdio.h>

int main(void)
{
  int a, b;

  a = printf("Hello there\n");
  printf("Printed %d characters\n", a);
  
  b = printf("Hi\twith a tab\n");
  printf("Printed %d characters\n", b);

  return 0;
}
